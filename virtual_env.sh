#!/usr/bin/sh

pip3 --version
mkdir virtual_env
python3 -m venv virtual_env
. virtual_env/bin/activate
pip3 install --upgrade pip
pip3 install -r requirements.txt
