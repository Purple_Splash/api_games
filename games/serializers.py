from rest_framework import serializers
from games.models import Game, Place


class GameSerializer(serializers.ModelSerializer):
    place_name = serializers.ReadOnlyField(source='place.name')

    class Meta:
        model = Game
        fields = '__all__'

class PlaceSerializer(serializers.ModelSerializer):
    games = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='title'
     )

    class Meta:
        model = Place
        fields = '__all__'
