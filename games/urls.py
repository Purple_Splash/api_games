from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from games import views

urlpatterns = [
    path('games/', views.GameList.as_view()),
    path('games/<int:pk>', views.GameDetail.as_view()),
    path('places/', views.PlaceList.as_view()),
    path('places/<int:pk>', views.PlaceDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)