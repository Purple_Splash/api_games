from django.db import models


class Place(models.Model):
    name = models.CharField(max_length=100, blank=True, default='')

    def __str__(self):
        return self.name

class Game(models.Model):
	title = models.CharField(max_length=100, blank=True, default='')
	genre = models.CharField(max_length=50, blank=True, default='')
	support = models.CharField(max_length=50, blank=True, default='')
	publisher = models.CharField(max_length=50, blank=True, default='')
	release = models.DateField(blank=True, null=True)
	place = models.ForeignKey(Place, related_name='games', on_delete=models.CASCADE, null=True)